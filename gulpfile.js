const gulp = require('gulp');
const util = require("gulp-util");
const sass = require("gulp-sass");
const autoprefixer = require('gulp-autoprefixer');
const minifycss = require('gulp-minify-css');
const rename = require('gulp-rename');
const log = util.log;

const sassFiles = './*.scss'

gulp.task("sass", function(){
  log("Generate CSS files " + (new Date()).toString());
  return gulp.src(sassFiles)
    .pipe(sass({ style: 'expanded' }))
    .pipe(autoprefixer("last 3 version","safari 5", "ie 8", "ie 9"))
    .pipe(gulp.dest("assets/css"))
    .pipe(rename({suffix: '.min'}))
    .pipe(minifycss())
    .pipe(gulp.dest('assets/css'));
});

gulp.task('default', function() {
  return console.log('default task');
});
